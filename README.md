# Uninformed sterilization git repository 

On the occasion of International Women's Day 2021, I calculated in this project the percentage of women in 40 countries that had been through uninformed sterilization, using DHS data. Uninformed sterilization is a violation of reproductive rights, and means that these women were sterilized but not told that the operation meant they could not have children anymore. The blog post associated with this project can be read [here](https://camillebelmin.github.io/post/uninformed_sterilization/).

The principle of the code is fairly easy. We loop over all survey and apply a function that calculates, for each survey, our indicator. But to be able to run this code, you need to 
- (i) have downloaded DHS data on your computer. You need to use the wonderful [rdhs package](https://cran.r-project.org/web/packages/rdhs/vignettes/introduction.html) to download the datasets in the right format (RDS). 
- (ii) have a metadata file where the DHS survey ID correspond to the file name you have on your computer. I provided my own metadata (/data/intermediary_files/metadata_dhs.csv) but because the DHS regularly updates the DHS data files and changes the file names, if you downloaded the DHS data recently all files names in the metadata_dhs.csv may not correspond to the file names on your computer. 
you can make your own metadata file by using the function rdhs::datasets(). Then you simply need to create a column called "file_name" that corresponds to the RDS file name, its very simple take the columns "ZIP_file_name" and change the end ".ZIP" to ".RDS" by using stringr::substr(). (I will try to upload the code for this soon!)

This folder contains: 

- uninformed_sterilization.Rmd --> the blog post that presents this small project and the approach. It's more reader-friendly on [my website](https://camillebelmin.github.io/post/uninformed_sterilization/)

- code/helper_functions_sterilization.R --> This script contains helper functions for the project on uninformed sterilization in DHS data.

- code/get_sterilization_data_dhs.R --> This script loops over all DHS survey listed in the file metadata_dhs.csv and calculates the percentage of women, in each country, that have been through "uninformed sterilization" 

- data/sterilization_dhs_data.csv -->  Data with the sterilization rates and uninformed sterilization rates, for all available countries

- data/women_not_told.csv --> Data with all the individual found in the survey that have been through uninformed sterilization. 
- data/intermediary_files/metadata_dhs.csv --> Metadata file with a mapping between DHS survey ID and the file name. 
